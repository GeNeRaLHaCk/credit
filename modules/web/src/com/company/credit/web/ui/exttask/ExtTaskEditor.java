/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */

package com.company.credit.web.ui.exttask;

import com.company.credit.entity.ExtTask;
import com.haulmont.cuba.core.entity.Entity;
import com.haulmont.thesis.web.ui.task.TaskEditor;

public class ExtTaskEditor extends TaskEditor<ExtTask> {

    @Override
    public void setItem(Entity item) {
        super.setItem(item);
    }
}
