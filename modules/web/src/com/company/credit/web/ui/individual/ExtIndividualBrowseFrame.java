/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */

package com.company.credit.web.ui.individual;

import com.company.credit.service.NewService;
import com.haulmont.cuba.gui.UiComponents;
import com.haulmont.cuba.gui.components.Component;
import com.haulmont.cuba.gui.components.Label;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.TextField;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.haulmont.thesis.core.entity.Individual;
import com.haulmont.thesis.web.ui.individual.IndividualBrowseFrame;

import javax.inject.Inject;
import java.util.Map;

public class ExtIndividualBrowseFrame extends IndividualBrowseFrame {

    @Inject
    private UiComponents uiComponents;
    @Inject
    protected ComponentsFactory componentsFactory;
    @Inject
    protected NewService individualService;

    private final String CREDIT_COUNT_COLUMN = "creditOrderCount";

    protected Component generateUserFieldComponent(Individual individual) {
        Label label = componentsFactory.createComponent(Label.class);
        label.setValue(individualService.getCreditCount(individual.getId()));
        return label;
    }

    @Override
    public void init(Map<String, Object> params) {
        super.init(params);
        individualsTable.addGeneratedColumn(CREDIT_COUNT_COLUMN, new Table.ColumnGenerator<Individual>() {
            public Component generateCell(Individual individual) {
                return generateUserFieldComponent(individual);
            }
        });
        //individualsTable.getColumn(CREDIT_COUNT_COLUMN);
    }
}
