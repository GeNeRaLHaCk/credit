/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */

package com.company.credit.web.ui.exttask;

import com.company.credit.entity.ExtTask;
import com.haulmont.thesis.web.ui.task.TaskBrowser;

/*
@UiController("credit$ExtTask.browse")*/
/*@UiDescriptor("task-browse.xml")
@LoadDataBeforeShow*/
public class ExtTaskBrowse extends TaskBrowser<ExtTask> {
}
