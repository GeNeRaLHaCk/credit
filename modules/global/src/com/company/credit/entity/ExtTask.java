/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */

package com.company.credit.entity;

import com.haulmont.cuba.core.entity.annotation.Extends;
import com.haulmont.thesis.core.entity.Task;

import javax.persistence.*;

@Table(name = "TM_TASK")
@Entity(name = "credit$ExtTask")
@Extends(Task.class)
@DiscriminatorValue("2200")
@PrimaryKeyJoinColumn(name = "CARD_ID")
public class ExtTask extends Task {
    private static final long serialVersionUID = 2104958099836675609L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREDIT_ORDER_ID")
    protected CreditOrder creditOrder;

    public CreditOrder getCreditOrder() {
        return creditOrder;
    }

    public void setCreditOrder(CreditOrder creditOrder) {
        this.creditOrder = creditOrder;
    }
}
