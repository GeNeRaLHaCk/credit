/* * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */


import com.company.credit.entity.CreditOrder
import org.apache.commons.lang3.BooleanUtils

CreditOrder onlineOrder = card

return BooleanUtils.isTrue(onlineOrder.paid)
