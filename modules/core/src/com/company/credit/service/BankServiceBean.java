/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */

package com.company.credit.service;

import com.haulmont.thesis.core.entity.Bank;
import org.springframework.stereotype.Service;
import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;

import javax.inject.Inject;

@Service(BankService.NAME)
public class BankServiceBean implements BankService {

    @Inject
    protected Persistence persistence;

    @Override
    public long getTotalCreditsAmountByBank(Bank bank) {
        Long result;
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            Query query = em.createQuery("select count(c) from credit$Credit c where c.bank = :bank");
            query.setParameter("bank", bank);
            result = (Long) query.getFirstResult();
            tx.commit();
        }
        return result != null ? result : 0;
    }
}
