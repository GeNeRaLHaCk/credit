/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */

package com.company.credit.service;

import com.haulmont.cuba.core.EntityManager;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Query;
import com.haulmont.cuba.core.Transaction;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.UUID;

@Service(NewService.NAME)
public class NewServiceBean implements NewService {

    @Inject
    private Persistence persistence;
    @Override
    public Long getCreditCount(UUID individualId) {
        Long result;
        try (Transaction tx = persistence.createTransaction()) {
            EntityManager em = persistence.getEntityManager();
            Query query = em.createQuery(
                    "select count(c) from credit$CreditOrder c where c.borrower.id = :individualId"
            );
            query.setParameter("individualId", individualId);
            result = (Long) query.getFirstResult();
            tx.commit();
        }
        return result != null ? result : 0;
    }
}
