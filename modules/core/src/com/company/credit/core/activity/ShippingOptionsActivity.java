/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */


package com.company.credit.core.activity;

import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.workflow.core.activity.ActivityHelper;
import com.haulmont.workflow.core.app.WfService;
import com.haulmont.workflow.core.entity.Card;
import org.jbpm.api.activity.ActivityExecution;
import org.jbpm.api.activity.ExternalActivityBehaviour;

import java.util.Map;

public class ShippingOptionsActivity implements ExternalActivityBehaviour {
    @Override
    public void signal(ActivityExecution execution, String signalName, Map<String, ?> parameters) throws Exception {
        execution.take(signalName);

        //варианты доставки пользователь будет определять сам(либо доставка либо самовывоз
        //процесорные переменные , когда процесс заканчивается, они грохаются
    }

    @Override
    public void execute(ActivityExecution execution) throws Exception {
        final Card card = ActivityHelper.findCard(execution);
        WfService wfService = AppBeans.get("workflow_wfService");
        Map<String,Object> params = wfService.getProcessVariables(card);

        if (params.containsKey("deliveryOptions")) {
            String deliveryOptions = (String) params.get("deliveryOptions");
            if (deliveryOptions.equals("Delivery")) {
                execution.take("Dostavka");
            }
        }
        execution.take("Samovyvoz");
        //
    }//в дизайнере пишем пакет и исполняемый класс
}
