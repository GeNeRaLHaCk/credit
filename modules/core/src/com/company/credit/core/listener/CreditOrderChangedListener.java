/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */

package com.company.credit.core.listener;

import com.haulmont.thesis.core.entity.Doc;
import com.haulmont.thesis.core.listener.DocEntityListener;
import org.springframework.stereotype.Component;

@Component("credit_CreditOrderChangedListener")
public class CreditOrderChangedListener extends DocEntityListener {

    @Override
    protected String createDescription(Doc entity) {
        return "!!!" + super.createDescription(entity);
    }
}
