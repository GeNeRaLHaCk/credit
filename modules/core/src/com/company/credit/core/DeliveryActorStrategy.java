/*
 * Copyright (c) 2023 LTD Haulmont Samara. All Rights Reserved.
 * Haulmont Samara proprietary and confidential.
 * Use is subject to license terms.
 */


package com.company.credit.core;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.thesis.core.app.defaultprocactors.DefaultProcessActorAssignmentStrategy;
import com.haulmont.thesis.core.entity.defaultactor.TsDefaultProcActor;
import com.haulmont.workflow.core.entity.Card;

import javax.annotation.Nullable;
import javax.inject.Inject;

public class DeliveryActorStrategy implements DefaultProcessActorAssignmentStrategy {

    @Inject
    private DataManager dataManager;

    //для любово бина мы сначла заводим:
    public static final String NAME = "bookstore_DeliveryActorStrategy";
    @Override
    public String getId() {
        return NAME;
    }

    @Nullable
    @Override
    public User getUser(TsDefaultProcActor defaultProcActor, Card card) {
        return null;
    }

    @Override
    public String getCaption() {
        return null;
    }
}
